# What is Zombie Apocalypse? #

ZA adds the possibility for a zombie apocalypse to start at 9:00 each night. Zombies will spawn around each player, and you must defeat the zombies before morning to receive a reward!
How does it work?

At 9:00 P.M. there is a configurable (default: 20%) chance for an apocalypse to start and a configurable amount of zombies (default: 40) will spawn and a configurable percent of them (default: 50%) need to be killed for a reward to be gained. The apocalypse can be started with a command (optionally you can specify a number of zombies per person to spawn). Each zombie will spawn 1-14 blocks away in both the X and Z directions, one of coordinates is offset by another 10 blocks so the zombies don't spawn too close.

The rewards are configurable: Each possible item needs to specify these things - (these are explained in config.yml)

1. Item name
1. Chance
1. Quantity
1. Message

Default Items
Item - Chance

* Diamond sword - 5
* Diamond chestplate - 5
* 2 x Diamond - 25
* Gold sword - 25
* Emerald - 25
* 1 x Diamond - 50
* 5 x Iron ingot - 150 

## Commands ##

    /za help - displays information about the plugin.
    /za version - displays the version of the plugin.
    /za commands - displays a list of all commands.
    /za kills - shows the kills/goal of the current apocalypse.

## OP commands: ##
    /za start - manually starts the zombie apocalypse.
    /za start <integer> - manually starts the zombie apocalypse with <integer> number of zombies spawned per person.
    za getitem - gives the name of the item in your hand, used for configuring rewards.
Configuration

Configuration of Zombie Apocalypse is very simple. In config.yml enter the name of your world in the world field and change false to true in the configured field. If you don't do this the plugin will not load and every time someone logs in it will say the mod needs to be configured. If the world name isn't valid and it says it's configured you will get some null pointer exceptions from two tasks. There is also a field called "numzombies" that has a default value of 40, this is the number of zombies that will spawn per person from both naturally occurring apocalypses and when using the command "/za start" without specifying a number.
As an example my server has a world named Trysnor so I set up the config.yml file like this

    #Enter the name of the world you want to use for the zombie apocalypse
    #Replace "world" with whatever your server's world is
    world: Trysnor
    #numzombies is the amount of zombies you want to spawn per person from a default event.
    numzombies: 40
    #when you have configured this file with your world's name change false to true
    configured: true

You can also configure the chance per night for the apocalypse to happen. There is a message at 8:00 PM warning that the apocalypse might be coming, and a message if it doesn't happen. Both of those messages can be disabled. There is also a limit to the number of zombies that can spawn. If this number is anything under one there is no limit. If the "hardlimit" is over one, it will spawn only up to that many zombies.
FOR SUGGESTIONS AND BUGS

Please create a ticket here: https://bitbucket.org/sheodox/zombieapocalypse/issues/new I'm not actively developing this mod because I'm working on other projects. If you would like to fork this mod there is a git repo on the repository tab.
Have fun and leave feedback!
-sheodox (aka dbs727)