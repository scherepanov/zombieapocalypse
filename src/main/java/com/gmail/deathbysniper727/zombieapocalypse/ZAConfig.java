package com.gmail.deathbysniper727.zombieapocalypse;

import java.util.Map;
import java.util.HashMap;

/**
 * ZAConfig
 */
public class ZAConfig {

    private org.bukkit.configuration.file.FileConfiguration config;
    private Map<String, String> defaultConfig = new HashMap<String, String>();

    /**
     * Class Constructor
     *
     * @param za ZombieApocalypse
     */
    public ZAConfig(ZombieApocalypse za)
    {
        this.config = za.getConfig();
        this._initializeDefault();
    }

    protected void _initializeDefault()
    {
        defaultConfig.put("comingMessage", "ZA: There are sounds coming from the ground, the dead might soon awake!");
        defaultConfig.put("safeMessage", "ZA: You are safe for another night, the undead still lie in their graves.");
        defaultConfig.put("beginMessage", "ZA: The horde of the undead have been let loose! Survive!");
        defaultConfig.put("nobodyMessage", "ZA: Nobody is around, the disappointed zombies return to their graves.");
        defaultConfig.put("runTime", "13000");
    }

    /**
     * Retrieve config value by key, and return default if value not defined
     *
     * @param key String
     * @return String
     */
    public String getString(String key)
    {
        String value = config.getString(key);

        return value.length() > 0 ? value : defaultConfig.get(key);
    }

    public boolean getBoolean(String key)
    {
        String value = config.getString(key);

        return Boolean.parseBoolean(value.length() > 0 ? value : defaultConfig.get(key));
    }

    public int getInt(String key)
    {
        String value = config.getString(key);

        return Integer.valueOf(value.length() > 0 ? value : defaultConfig.get(key));
    }

    public double getDouble(String key)
    {
        String value = config.getString(key);

        return Double.valueOf(value.length() > 0 ? value : defaultConfig.get(key));
    }

}
